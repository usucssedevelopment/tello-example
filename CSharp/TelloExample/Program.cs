﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TelloExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var droneAddress = "192.168.10.1";
            var dronePort = 8889;
            var udpClient = new UdpClient();
            udpClient.Connect(droneAddress, dronePort);
            udpClient.Client.ReceiveTimeout = 10000;

            //-------- Put drone in command mode ---------

            Console.WriteLine("Put drone in command mode...");

            byte[] bytesToSend;
            byte[] bytesReceived;
            IPEndPoint remoteEndPoint;

            string request;
            string reply = null;
            var maxRetries = 3;
            while (maxRetries > 0)
            {
                request = "command";
                bytesToSend = Encoding.UTF8.GetBytes(request);
                udpClient.Send(bytesToSend, bytesToSend.Length);
                Console.WriteLine($"Sent {request} ({bytesToSend.Length} bytes) to {droneAddress}:{dronePort}");

                remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                try
                {
                    bytesReceived = udpClient.Receive(ref remoteEndPoint);
                }
                catch (SocketException e)
                {
                    if (e.SocketErrorCode == SocketError.TimedOut)
                    {
                        Console.WriteLine("Time out on receive");
                        bytesReceived = null;
                    }
                    else
                        throw e;
                }
                if (bytesReceived!=null)
                {
                    reply = Encoding.UTF8.GetString(bytesReceived);
                    Console.WriteLine($"Received {bytesToSend.Length} bytes from {remoteEndPoint}: {reply}");
                    if (reply=="ok")
                        break;
                }
                Console.WriteLine("Remaining retries: " + maxRetries);
                maxRetries--;
            }
            if (reply == null || reply!="ok")
            {
                Console.WriteLine("Exiting because of no or bad reply");
                return;
            }

            Thread.Sleep(1000);

            //-------- take off ---------

            Console.WriteLine("Takeoff...");

            request = "takeoff";
            bytesToSend = Encoding.UTF8.GetBytes(request);
            udpClient.Send(bytesToSend, bytesToSend.Length);
            Console.WriteLine($"Sent {request} ({bytesToSend.Length} bytes) to {droneAddress}:{dronePort}");

            reply = null;
            remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                bytesReceived = udpClient.Receive(ref remoteEndPoint);
            }
            catch (SocketException e)
            {
                if (e.SocketErrorCode == SocketError.TimedOut)
                {
                    Console.WriteLine("Time out on receive");
                    bytesReceived = null;
                }
                else
                    throw e;
            }
            if (bytesReceived != null)
            {
                reply = Encoding.UTF8.GetString(bytesReceived);
                Console.WriteLine($"Received {bytesToSend.Length} bytes from {remoteEndPoint}: {reply}");
            }
            if (reply == null || reply != "ok")
            {
                Console.WriteLine("Exiting because of no or bad reply");
                return;
            }

            Thread.Sleep(1000);

            //-------- rotate ---------

            Console.WriteLine("Rotate...");

            request = "cw 360";
            bytesToSend = Encoding.UTF8.GetBytes(request);
            udpClient.Send(bytesToSend, bytesToSend.Length);
            Console.WriteLine($"Sent {request} ({bytesToSend.Length} bytes) to {droneAddress}:{dronePort}");

            reply = null;
            remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                bytesReceived = udpClient.Receive(ref remoteEndPoint);
            }
            catch (SocketException e)
            {
                if (e.SocketErrorCode == SocketError.TimedOut)
                {
                    Console.WriteLine("Time out on receive");
                    bytesReceived = null;
                }
                else
                    throw e;
            }
            if (bytesReceived != null)
            {
                reply = Encoding.UTF8.GetString(bytesReceived);
                Console.WriteLine($"Received {bytesToSend.Length} bytes from {remoteEndPoint}: {reply}");
            }
            if (reply == null || reply != "ok")
            {
                Console.WriteLine("Exiting because of no or bad reply");
                return;
            }

            Thread.Sleep(1000);

            //-------- land ---------

            request = "land";
            bytesToSend = Encoding.UTF8.GetBytes(request);
            udpClient.Send(bytesToSend, bytesToSend.Length);
            Console.WriteLine($"Sent {request} ({bytesToSend.Length} bytes) to {droneAddress}:{dronePort}");

            reply = null;
            remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                bytesReceived = udpClient.Receive(ref remoteEndPoint);
            }
            catch (SocketException e)
            {
                if (e.SocketErrorCode == SocketError.TimedOut)
                {
                    Console.WriteLine("Time out on receive");
                    bytesReceived = null;
                }
                else
                    throw e;
            }
            if (bytesReceived != null)
            {
                reply = Encoding.UTF8.GetString(bytesReceived);
                Console.WriteLine($"Received {bytesToSend.Length} bytes from {remoteEndPoint}: {reply}");
            }
            if (reply == null || reply != "ok")
            {
                Console.WriteLine("Exiting because of no or bad reply");
                return;
            }

            Console.WriteLine("Done");
        }

    }
}

