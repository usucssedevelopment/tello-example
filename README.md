# tello-example #

This repository contains a simple example of how to communicate with
a Tello Drone, implemented in both Java and C#. Although functional,
this program is intentionally not designed very well.  It suffers
from several obvious programming pitfalls and does not follow the
principles abstraction, modularity, and encapsulation.

## Contribution guidelines ##

* Do a code review with Stephen Clyde or some other administrator assigned to the project.

## Who do I talk to? ##

* Stephen Clyde, Utah State University, Stephen.Clyde@usu.edu, 435-764-1596
