import java.net.*;
import java.nio.charset.StandardCharsets;

/**
 * This is a simple example of how to control a Tello Edu Drone.
 *
 * Note: This example simply aims to illustrate the opening of a DatagramChannel and the sending and receiving
 * of messages.  It does NOT represent a good design.  It has poor modularity and virtually not meaning abstractions.
 */
public class TelloExample {

    @SuppressWarnings("Duplicates")
    public static void main(String[] args) throws Exception {

        InetAddress droneAddress = InetAddress.getByName("192.168.10.1");
        int dronePort = 8889;

        DatagramSocket udpClient =  new DatagramSocket();
        udpClient.setSoTimeout(10000);

        //-------- Put drone in command mode ---------

        System.out.println("Put drone in command mode...");

        String request = "command";
        byte[] bytesToSent;
        byte[] bytesReceived;
        DatagramPacket datagramPacket;

        String reply = null;
        int maxRetries = 3;
        while (maxRetries > 0) {

            bytesToSent = request.getBytes(StandardCharsets.UTF_8);
            datagramPacket = new DatagramPacket(bytesToSent, bytesToSent.length, droneAddress, dronePort);
            udpClient.send(datagramPacket);
            System.out.println("Sent " + request + " bytes to " + droneAddress.toString() + ":" + dronePort);

            bytesReceived = new byte[64];
            datagramPacket = new DatagramPacket(bytesReceived, 64);
            try {
                udpClient.receive(datagramPacket);
            }
            catch (SocketTimeoutException ex) {
                datagramPacket = null;
            }
            if (datagramPacket != null) {
                System.out.println(String.format("Received %d bytes", datagramPacket.getLength()));
                reply = new String(bytesReceived, 0, datagramPacket.getLength(), StandardCharsets.UTF_8);
                System.out.println("Receive " + reply);
                if (reply.equals("ok")) {
                    break;
                }
            }
            System.out.println("Remaining retries: " + maxRetries);
            maxRetries--;
        }

        if (reply == null || !reply.equals("ok")) {
            System.out.println("Existing because no or bad reply received");
            return;
        }

        Thread.sleep(1000);

        //-------- take off ---------

        System.out.println("Takeoff...");

        request = "takeoff";
        bytesToSent = request.getBytes(StandardCharsets.UTF_8);
        datagramPacket = new DatagramPacket(bytesToSent, bytesToSent.length, droneAddress, dronePort);
        udpClient.send(datagramPacket);
        System.out.println("Sent " + request + " bytes to " + droneAddress.toString() + ":" + dronePort);

        reply = null;
        bytesReceived = new byte[64];
        datagramPacket = new DatagramPacket(bytesReceived, 64);
        try {
            udpClient.receive(datagramPacket);
        }
        catch (SocketTimeoutException ex) {
            datagramPacket = null;
        }
        if (datagramPacket != null) {
            System.out.println(String.format("Received %d bytes", datagramPacket.getLength()));
            reply = new String(bytesReceived, 0, datagramPacket.getLength(), StandardCharsets.UTF_8);
            System.out.println("Receive " + reply);
        }

        if (reply == null || !reply.equals("ok")) {
            System.out.println("Existing because no or bad reply received");
            return;
        }

        Thread.sleep(1000);

        //-------- rotate ---------

        System.out.println("Rotate...");

        request = "cw 360";
        bytesToSent = request.getBytes(StandardCharsets.UTF_8);
        datagramPacket = new DatagramPacket(bytesToSent, bytesToSent.length, droneAddress, dronePort);
        udpClient.send(datagramPacket);
        System.out.println("Sent " + request + " bytes to " + droneAddress.toString() + ":" + dronePort);

        reply = null;
        bytesReceived = new byte[64];
        datagramPacket = new DatagramPacket(bytesReceived, 64);
        try {
            udpClient.receive(datagramPacket);
        }
        catch (SocketTimeoutException ex) {
            datagramPacket = null;
        }
        if (datagramPacket != null) {
            System.out.println(String.format("Received %d bytes", datagramPacket.getLength()));
            reply = new String(bytesReceived, 0, datagramPacket.getLength(), StandardCharsets.UTF_8);
            System.out.println("Receive " + reply);
        }

        if (reply == null || !reply.equals("ok")) {
            System.out.println("Existing because no or bad reply received");
            return;
        }

        Thread.sleep(1000);

        //-------- land ---------

        reply = null;
        request = "land";
        bytesToSent = request.getBytes(StandardCharsets.UTF_8);
        datagramPacket = new DatagramPacket(bytesToSent, bytesToSent.length, droneAddress, dronePort);
        udpClient.send(datagramPacket);
        System.out.println("Sent " + request + " bytes to " + droneAddress.toString() + ":" + dronePort);

        bytesReceived = new byte[64];
        datagramPacket = new DatagramPacket(bytesReceived, 64);
        try {
            udpClient.receive(datagramPacket);
        }
        catch (SocketTimeoutException ex) {
            datagramPacket = null;
        }
        if (datagramPacket != null) {
            System.out.println(String.format("Received %d bytes", datagramPacket.getLength()));
            reply = new String(bytesReceived, 0, datagramPacket.getLength(), StandardCharsets.UTF_8);
            System.out.println("Received " + reply);
        }

        if (reply == null || !reply.equals("ok")) {
            System.out.println("Existing because no or bad reply received");
            return;
        }

        System.out.println("Done");
    }

}

